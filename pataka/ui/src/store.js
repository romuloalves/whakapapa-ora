import Vue from 'vue'
import Vuex from 'vuex'
// import set from 'lodash.set'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {}
})
